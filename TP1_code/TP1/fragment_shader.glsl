#version 330 core

// Ouput data
in vec3 o_positionWorld;
in vec3 o_normalWorld;
in vec2 o_uv0;
out vec4 FragColor;

in float altitude;

uniform sampler2D colorTexture;
uniform sampler2D grass;
uniform sampler2D rock;
uniform sampler2D snow;

uniform vec3 barColor;
uniform int isHealthBar;
uniform int isPoulePart;

void main(){
    //FragColor = texture(colorTexture, o_uv0);

    /*if (altitude < (0.25*0.2)) {
        FragColor = texture2D(grass, o_uv0);
    } else if (altitude < (0.5*0.2)) {
        FragColor = texture2D(rock, o_uv0);
    } else {
        FragColor = texture2D(snow, o_uv0);
    }*/

    vec4 texel1 = texture(grass, o_uv0);
    vec4 texel2 = texture(rock, o_uv0);
    vec4 texel3 = texture(colorTexture, o_uv0);

    // FragColor pour le sol (hauteur à 0 et profondeur > -4)
    if (o_positionWorld.y == 0.0f && o_positionWorld.z > -4.0f) {
        FragColor = texel1;
    }
    // FragColor pour le mur (hauteur > 0 et profondeur à -4)
    else if (o_positionWorld.y > 0.0f && o_positionWorld.z == -4.0f) {
        FragColor = texel2;
    }
    else if (isHealthBar == 1) {
        FragColor = vec4(barColor, 1.0);
    }
    else if(isPoulePart == 1 || isPoulePart == 2 || isPoulePart == 4 || isPoulePart == 5){
        FragColor = vec4(1.0, 1.0, 1.0, 1.0);
    }
    else if(isPoulePart == 7 || isPoulePart == 6 || isPoulePart == 3){
        FragColor = vec4(1.0, 0.8, 0.0, 1.0);
    }
    else if(isPoulePart > 7){
        FragColor = vec4(1.0, 0.0, 0.0, 1.0);
    }
    // FragColor pour le cube (sinon)
    else {
        FragColor = texel3;
    }

    //DEBUG:
    //FragColor = vec4(o_uv0,1.0,1.0);
    //FragColor = vec4(o_positionWorld,1.0);
}
