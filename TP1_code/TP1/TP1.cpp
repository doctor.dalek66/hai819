// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <iostream>
#include <random>
#include <list> //std::list
#include <memory> //std::unique_ptr
#include <algorithm>

// Include GLEW
#include <GL/glew.h>

// Include GLFW
#include <GLFW/glfw3.h>

#define STB_IMAGE_IMPLEMENTATION

#include "stb_image.h"

GLFWwindow *window;

// Include GLM
#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <iostream>

using namespace glm;

#include <common/shader.hpp>
#include <common/objloader.hpp>
#include <common/texture.hpp>
#include <common/vboindexer.hpp>
#include <common/controls.hpp>

void processInput(GLFWwindow *window);

// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

// camera
glm::vec3 camera_position = glm::vec3(0.0f, 1.0f, 3.0f);
glm::vec3 camera_target = glm::vec3(0.0f, 0.0f, -1.0f);
glm::vec3 camera_up = glm::vec3(0.0f, 1.0f, 0.0f);

glm::vec3 object_position = glm::vec3(0.0f, 0.0f, 0.0f); // Position de l'objet 3D
float distanceFromCamera = glm::length(camera_position - object_position);

// timing
float deltaTime = 0.0f;    // time between current frame and last frame
float lastFrame = 0.0f;

//rotation
float angle = 0.;
float zoom = 1.;

//resolution
float resolution = 25.0f;

//ressources mod3d
std::vector<unsigned short> indices; //Triangles concaténés dans une liste
std::vector <std::vector<unsigned short>> triangles;
std::vector <glm::vec3> indexed_vertices;
std::vector <glm::vec2> uvs;

//rotation camera
float rotation_speed = 0.1f;
float rotation_angle = 0.0f;
bool is_free_camera = true;
float sun_rotation = 0.0f;
float arm_rotation = 0.0f;

glm::vec3 gravity(0.0f, -9.81f, 0.0f);
bool updateRotations = false;

bool shotFired = true; //tirer un seul cube à la fois
int HpPoule = 100;


/**
 * classe transform qui sera appliqué lors de la création des noeuds
 */
class Transform {
public:
    glm::vec3 s; // non-uniform scale
    glm::mat3x3 r; // rotation
    glm::vec3 t; // translation
    glm::vec3 rotationCenter; // center of rotation

    Transform(glm::vec3 scale, glm::mat3x3 rotation, glm::vec3 translation)
            : s(scale), r(rotation), t(translation), rotationCenter(glm::vec3(0.0f)) {}

    Transform(glm::vec3 scale, glm::mat3x3 rotation, glm::vec3 translation, glm::vec3 center)
            : s(scale), r(rotation), t(translation), rotationCenter(center) {}

    Transform mix_with(Transform b, float k) const {
        Transform result(this->s, this->r, this->t);
        result.s = this->s * k + b.s * (1 - k);
        result.r = glm::mix(this->r, b.r, k);
        result.t = this->t * k + b.t * (1 - k);
        result.rotationCenter = this->rotationCenter * k + b.rotationCenter * (1 - k);
        return result;
    }

    Transform combine(const Transform &other) const {
        Transform result(*this);
        result.s = this->s * other.s;
        result.r = this->r * other.r;
        result.t = this->t + other.t;
        return result;
    }

    // Apply the transformation on a point
    glm::vec3 applyToPoint(glm::vec3 point) {
        glm::vec3 translatedPoint = point - rotationCenter;
        return this->s * (this->r * translatedPoint) + this->t + rotationCenter;
    }

    // Apply the transformation on a vector
    glm::vec3 applyToVector(glm::vec3 vector) {
        return this->s * (this->r * vector);
    }

    // Apply the transformation on a versor
    glm::vec3 applyToVersor(glm::vec3 versor) {
        return this->r * versor;
    }

    glm::mat4 getMatrix() {
        glm::mat4 modelMatrix = glm::mat4(1.0f);
        modelMatrix = glm::scale(modelMatrix, glm::vec3(s));
        modelMatrix = glm::translate(modelMatrix, t * glm::vec3(1.0f / s.x, 1.0f / s.y, 1.0f / s.z));
        modelMatrix *= glm::mat4(r);


        glm::mat4 translationMatrix = glm::translate(glm::mat4(1.0f), rotationCenter);
        glm::mat4 inverseTranslationMatrix = glm::translate(glm::mat4(1.0f), -rotationCenter);

        modelMatrix = translationMatrix * modelMatrix * inverseTranslationMatrix;

        return modelMatrix;
    }

};

/**
 * structure boite englobante alignée sur les axes
 */
struct AABB {
    glm::vec3 min; // Coordonnées du coin inférieur gauche de la boîte
    glm::vec3 max; // Coordonnées du coin supérieur droit de la boîte

    AABB() = default;

    AABB(glm::vec3 min, glm::vec3 max) : min(min), max(max) {}
};

/**
 * une fonction pour vérifier si deux boîtes AABB sont en collision
 * @param box1
 * @param box2
 * @return true si les boîtes se chevauchent, sinon false.
 */
bool checkAABBCollision(const AABB &box1, const AABB &box2) {
    return (box1.min.x <= box2.max.x && box1.max.x >= box2.min.x) &&
           (box1.min.y <= box2.max.y && box1.max.y >= box2.min.y) &&
           (box1.min.z <= box2.max.z && box1.max.z >= box2.min.z);
}


int width, height, nrChannels;
unsigned char *data = stbi_load("heightmap.png", &width, &height, &nrChannels, 0);

/*******************************************************************************/

/**
 * fonction de création d'une surface
 * @param indices
 * @param triangles
 * @param indexed_vertices
 * @param uvs
 * @param resolution
 * @param detailFactor paramètre utilisé pour le LOD
 */
void surfacePlane(std::vector<unsigned short> &indices, std::vector <std::vector<unsigned short>> &triangles,
                  std::vector <glm::vec3> &indexed_vertices, std::vector <glm::vec2> &uvs, float resolution,
                  float detailFactor) {
    double size = 1.0; //unused for now
    indices.resize(0);
    triangles.resize(0);
    indexed_vertices.resize(0);

    resolution = resolution * detailFactor;

    for (int i = 0; i < resolution; i++) {
        for (int j = 0; j < resolution; j++) {
            indexed_vertices.push_back(
                    glm::vec3((float) i / resolution, 0, (float) j / resolution));
        }
    }

    //création uvs
    for (int i = 0; i < resolution; i++) {
        for (int j = 0; j < resolution; j++) {
            uvs.push_back(glm::vec2((float) i / (resolution - 1), (float) j / (resolution - 1)));
        }
    }

    //création triangles
    std::vector<unsigned short> triangleFactory(3);
    for (int i = 0; i < resolution - 1; i++) {
        for (int j = 0; j < resolution - 1; j++) {
            indices.push_back(i * resolution + j);
            indices.push_back(i * resolution + (j + 1));
            indices.push_back((i + 1) * resolution + j);
            triangleFactory[0] = i * resolution + j;
            triangleFactory[1] = i * resolution + (j + 1);
            triangleFactory[2] = (i + 1) * resolution + j;
            triangles.push_back(triangleFactory);
            indices.push_back((i + 1) * resolution + j);
            indices.push_back(i * resolution + (j + 1));
            indices.push_back((i + 1) * resolution + (j + 1));
            triangleFactory[0] = (i + 1) * resolution + j;
            triangleFactory[1] = i * resolution + (j + 1);
            triangleFactory[2] = (i + 1) * resolution + (j + 1);
            triangles.push_back(triangleFactory);
        }
    }
}

/**
 * fonction de création d'une sphère
 * @param indices
 * @param triangles
 * @param indexed_vertices
 * @param uvs
 * @param resolution
 * @param size taille de la sphère
 * @param detailFactor paramètre utilisé pour le LOD
 */
void newSphere(std::vector<unsigned short> &indices, std::vector <std::vector<unsigned short>> &triangles,
               std::vector <glm::vec3> &indexed_vertices, std::vector <glm::vec2> &uvs, float resolution, float size,
               float detailFactor) {

    indices.resize(0);
    triangles.resize(0);
    indexed_vertices.resize(0);

    resolution = resolution * detailFactor;

    int num_vertices = (resolution + 1) * (resolution + 1);
    indexed_vertices.reserve(num_vertices);

    float pi = glm::pi<float>();
    float theta_step = 2.0f * pi / resolution;
    float phi_step = pi / resolution;

    for (int i = 0; i <= resolution; i++) {
        float theta = i * theta_step;

        for (int j = 0; j <= resolution; j++) {
            float phi = j * phi_step;
            float x = sin(phi) * cos(theta);
            float y = sin(phi) * sin(theta);
            float z = cos(phi);
            indexed_vertices.push_back(glm::vec3(x * size, y * size, z * size));

            glm::vec2 uv((float) i / resolution, (float) j / resolution);
            uvs.push_back(uv);
        }
    }

    for (int i = 0; i < resolution; i++) {
        for (int j = 0; j < resolution; j++) {
            int a = i * (resolution + 1) + j;
            int b = (i + 1) * (resolution + 1) + j;
            int c = (i + 1) * (resolution + 1) + j + 1;
            int d = i * (resolution + 1) + j + 1;
            triangles.push_back(
                    {static_cast<unsigned short>(a), static_cast<unsigned short>(b), static_cast<unsigned short>(d)});
            triangles.push_back(
                    {static_cast<unsigned short>(b), static_cast<unsigned short>(c), static_cast<unsigned short>(d)});
        }
    }

    // On met à jour les indices
    for (const auto &triangle: triangles) {
        for (int i = 0; i < 3; i++) {
            indices.push_back(triangle[i]);
        }
    }
}

struct CubeProperties {
    glm::vec3 velocity;
    float weight;
    glm::vec3 acceleration;

    CubeProperties(glm::vec3 velocity, float weight, glm::vec3 acceleration)
            : velocity(velocity), weight(weight), acceleration(acceleration) {}
};

/**
 * fonction de création d'un cube
 * @param indices
 * @param triangles
 * @param indexed_vertices
 * @param uvs
 * @param size taille du cube
 * @param detailFactor paramètre utilisé pour le LOD
 */
void newCube(std::vector<unsigned short> &indices, std::vector <std::vector<unsigned short>> &triangles,
             std::vector <glm::vec3> &indexed_vertices, std::vector <glm::vec2> &uvs, float size, float detailFactor,
             CubeProperties *cubeProperties) {
    indices.resize(0);
    triangles.resize(0);
    indexed_vertices.resize(0);

    int num_vertices = 8;
    indexed_vertices.reserve(num_vertices);

    // Coordonnées des sommets du cube
    glm::vec3 vertices[6][4] = {
            {{-0.5f, -0.5f, 0.5f},  {0.5f,  -0.5f, 0.5f},  {0.5f,  0.5f,  0.5f},  {-0.5f, 0.5f,  0.5f}},
            {{0.5f,  -0.5f, 0.5f},  {0.5f,  -0.5f, -0.5f}, {0.5f,  0.5f,  -0.5f}, {0.5f,  0.5f,  0.5f}},
            {{0.5f,  -0.5f, -0.5f}, {-0.5f, -0.5f, -0.5f}, {-0.5f, 0.5f,  -0.5f}, {0.5f,  0.5f,  -0.5f}},
            {{-0.5f, -0.5f, -0.5f}, {-0.5f, -0.5f, 0.5f},  {-0.5f, 0.5f,  0.5f},  {-0.5f, 0.5f,  -0.5f}},
            {{-0.5f, 0.5f,  0.5f},  {0.5f,  0.5f,  0.5f},  {0.5f,  0.5f,  -0.5f}, {-0.5f, 0.5f,  -0.5f}},
            {{-0.5f, -0.5f, -0.5f}, {0.5f,  -0.5f, -0.5f}, {0.5f,  -0.5f, 0.5f},  {-0.5f, -0.5f, 0.5f}}
    };

    // Coordonnées de texture pour chaque face
    glm::vec2 faceUVs[6][4] = {
            {{0.0f, 0.0f}, {1.0f, 0.0f}, {1.0f, 1.0f}, {0.0f, 1.0f}}, // Face avant
            {{0.0f, 0.0f}, {1.0f, 0.0f}, {1.0f, 1.0f}, {0.0f, 1.0f}}, // Face droite
            {{0.0f, 0.0f}, {1.0f, 0.0f}, {1.0f, 1.0f}, {0.0f, 1.0f}}, // Face arrière
            {{0.0f, 0.0f}, {1.0f, 0.0f}, {1.0f, 1.0f}, {0.0f, 1.0f}}, // Face gauche
            {{0.0f, 1.0f}, {1.0f, 1.0f}, {1.0f, 0.0f}, {0.0f, 0.0f}}, // Face du dessus
            {{0.0f, 1.0f}, {1.0f, 1.0f}, {1.0f, 0.0f}, {0.0f, 0.0f}}  // Face du dessous
    };


    // Ajouter les sommets et les coordonnées de texture pour chaque face à indexed_vertices et uvs
    for (int i = 0; i < 6; i++) {
        for (int j = 0; j < 4; j++) {
            indexed_vertices.push_back(vertices[i][j] * size);
            uvs.push_back(faceUVs[i][j]);
        }
    }

    // Indices des triangles formant les faces du cube
    unsigned short faceIndices[6][6] = {
            {0,  1,  2,  2,  3,  0},
            {4,  5,  6,  6,  7,  4},
            {8,  9,  10, 10, 11, 8},
            {12, 13, 14, 14, 15, 12},
            {16, 17, 18, 18, 19, 16},
            {20, 21, 22, 22, 23, 20}

    };

    // Ajouter les indices des triangles aux tableaux 'triangles' et 'indices'
    for (int i = 0; i < 6; i++) {
        std::vector<unsigned short> triangle1 = {faceIndices[i][0], faceIndices[i][1], faceIndices[i][2]};
        std::vector<unsigned short> triangle2 = {faceIndices[i][3], faceIndices[i][4], faceIndices[i][5]};
        triangles.push_back(triangle1);
        triangles.push_back(triangle2);
    }

    // Mettre à jour les indices
    for (const auto &triangle: triangles) {
        for (int i = 0; i < 3; i++) {
            indices.push_back(triangle[i]);
        }
    }
}


/**
 * noeud du graphe de scène
 */
struct Node {
    std::vector<Node *> children;
    Transform transform;
    bool isSphere, isSurface, isCube;
    float sphereSize, cubeSize;
    std::vector<unsigned short> indices;
    std::vector <std::vector<unsigned short>> triangles;
    std::vector <glm::vec3> indexed_vertices;
    std::vector <glm::vec2> uvs;
    CubeProperties *cubeProperties;
    AABB aabb;
    glm::vec3 surfaceOrientation;
    bool active;

    Node(Transform transform, bool isSphere, bool isSurface, bool isCube, float size,
         CubeProperties *cubeProperties = nullptr)
            : transform(transform), isSphere(isSphere), isSurface(isSurface), isCube(isCube),
              sphereSize(isSphere ? size : 0.0f), cubeSize(isCube ? size : 0.0f), cubeProperties(cubeProperties), active(true) {
        aabb.min = transform.t - glm::vec3(size / 2.0f) * transform.s;
        aabb.max = transform.t + glm::vec3(size / 2.0f) * transform.s;
    }

    Node(Transform transform, bool isSphere, bool isSurface, bool isCube, glm::vec3 surfaceOrientation)
            : transform(transform), isSphere(isSphere), isSurface(isSurface), isCube(isCube),
              surfaceOrientation(surfaceOrientation), active(true) {
        glm::vec3 size = transform.s;
        glm::vec3 minLocal = -size / 2.0f;
        glm::vec3 maxLocal = size / 2.0f;

        glm::vec3 minWorld = transform.t + minLocal;
        glm::vec3 maxWorld = transform.t + maxLocal;

        // Rotate the AABB
        glm::mat4 rotationMat = glm::mat4(transform.r);
        minWorld = glm::vec3(rotationMat * glm::vec4(minWorld, 1.0f));
        maxWorld = glm::vec3(rotationMat * glm::vec4(maxWorld, 1.0f));

        aabb.min = glm::min(minWorld, maxWorld);
        aabb.max = glm::max(minWorld, maxWorld);
    }

    Node(Transform transform, bool isSphere, bool isSurface, bool isCube)
            : transform(transform), isSphere(isSphere), isSurface(isSurface), isCube(isCube),
              sphereSize(0.0f), cubeSize(0.0f), cubeProperties(nullptr), active(true) {
        float size = 0.0f; // Définissez une taille appropriée pour les cas où ni isSphere, ni isSurface, ni isCube ne sont vrais
        aabb.min = transform.t - glm::vec3(size / 2.0f) * transform.s;
        aabb.max = transform.t + glm::vec3(size / 2.0f) * transform.s;
    }


    float distanceFromCamera(const glm::vec3 &camera_position) const {
        glm::vec3 node_position = transform.t;
        return glm::length(camera_position - node_position);
    }
};


Transform getAbsoluteTransform(Node *node, Node *targetNode, const Transform &parentTransform) {
    // Calcule la transformation du nœud en prenant en compte la transformation du parent.
    Transform nodeTransform = parentTransform.combine(node->transform);

    if (node == targetNode) {
        // Si le nœud actuel est le nœud cible (le bras dans votre cas), renvoie sa transformation absolue.
        //std::cout << "trouvé: " << nodeTransform.t.x << " " << nodeTransform.t.y << " " << nodeTransform.t.z << std::endl;
        return nodeTransform;
    }

    // Parcourir les enfants et récursivement calculer leur transformation absolue.
    for (Node *child: node->children) {
        Transform childTransform = getAbsoluteTransform(child, targetNode, nodeTransform);
        if (childTransform.t != glm::vec3(0.0f) || childTransform.r != glm::mat3x3(0.0f) ||
            childTransform.s != glm::vec3(0.0f)) {
            // Si la transformation de l'enfant est différente de l'identité, cela signifie que le nœud cible a été trouvé.
            //std::cout << "trouvé enfant" << std::endl;
            return childTransform;
        }
    }

    // Si le nœud cible n'a pas été trouvé, renvoie une transformation identité.
    return Transform(glm::vec3(0.0f), glm::mat3x3(0.0f), glm::vec3(0.0f));
}

/**
 * le graphe de scène
 */
struct GrapheScene {
    Node *root;
    Node *barreVie;
    std::vector<Node*> pouleParts;
    GLuint shader;
    glm::mat4 viewMatrix;
    glm::mat4 projectionMatrix;
    std::vector<Node *> platforms;
    GLint isHealthBarLoc;
    GLint isPoulePartLoc;

    GrapheScene(Node *rootNode, GLuint shaderProgram) : root(rootNode), shader(shaderProgram) {}

    void ajoutNode(Node *node) {
        root->children.push_back(node);
    }

    void ajoutNodeAParent(Node *parentNode, Node *childNode) {
        parentNode->children.push_back(childNode);
    }

    void ajoutPlateforme(Node *node) {
        platforms.push_back(node);
        ajoutNode(node);
    }

    void supprimerNode(Node *node) {
        //supprimerNodeDeParent(root, node);
        node->active = false;
    }

    void update(const glm::mat4 &viewMatrix, const glm::mat4 &projectionMatrix) {
        this->viewMatrix = viewMatrix;
        this->projectionMatrix = projectionMatrix;
    }

    //afficher la scène
    void render(Node *node, glm::mat4 &parent_transform) {
        isHealthBarLoc = glGetUniformLocation(this->shader, "isHealthBar");
        isPoulePartLoc = glGetUniformLocation(this->shader, "isPoulePart");
        glUniform1i(isHealthBarLoc, 0);
        glUniform1i(isPoulePartLoc, 0);
        if(!node->active){
            return;
        }
        glm::mat4 model_matrix = parent_transform * node->transform.getMatrix();

        if (node == this->barreVie) {
            // Calcul de la couleur de la barre de vie
            glm::vec3 barColor;
            if (HpPoule > 50) {
                barColor = glm::vec3(0.0f, 1.0f, 0.0f); // Vert si plus de 50 HP
            } else if (HpPoule > 20) {
                barColor = glm::vec3(1.0f, 1.0f, 0.0f); // Jaune si entre 20 et 50 HP
            } else {
                barColor = glm::vec3(1.0f, 0.0f, 0.0f); // Rouge si moins de 20 HP
            }

            // Mise à jour de la couleur de la barre de vie dans le shader
            GLint barColorLoc = glGetUniformLocation(this->shader, "barColor");
            glUniform3fv(barColorLoc, 1, glm::value_ptr(barColor));
            glUniform1i(isHealthBarLoc, 1);
        }

        for(int i = 0; i < pouleParts.size(); i++){
            if(node == this->pouleParts.at(i)){
                glUniform1i(isPoulePartLoc, i+1);
                break;
            }
        }

        float detailFactor = 1.0f;
        float distanceFromCamera = node->distanceFromCamera(camera_position);
        if (distanceFromCamera > 10.0f) {
            detailFactor = 0.5f;
        } else if (distanceFromCamera > 20.0f) {
            detailFactor = 0.25f;
        }

        //affichage différent selon le type de node (appel de la fonction de création du type de node)
        if (node->isSphere) {
            std::vector<unsigned short> indices = node->indices; //Triangles concaténés dans une liste
            std::vector <std::vector<unsigned short>> triangles = node->triangles;
            std::vector <glm::vec3> indexed_vertices = node->indexed_vertices;
            std::vector <glm::vec2> uvs = node->uvs;

            newSphere(indices, triangles, indexed_vertices, uvs, resolution, node->sphereSize, detailFactor);

            // Load it into a VBO
            GLuint vertexbuffer;
            glGenBuffers(1, &vertexbuffer);
            glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
            glBufferData(GL_ARRAY_BUFFER, indexed_vertices.size() * sizeof(glm::vec3), &indexed_vertices[0],
                         GL_STATIC_DRAW);

            // Generate a buffer for the indices as well
            GLuint elementbuffer;
            glGenBuffers(1, &elementbuffer);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);

            GLuint uvbuffer;
            glGenBuffers(1, &uvbuffer);
            glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
            glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);

            glEnableVertexAttribArray(0);
            glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);

            glEnableVertexAttribArray(1);
            glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
            glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void *) 0);

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

            GLuint modelMatrixID = glGetUniformLocation(this->shader, "model");
            glUniformMatrix4fv(modelMatrixID, 1, GL_FALSE, &model_matrix[0][0]);

            glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_SHORT, (void *) 0);

            glDisableVertexAttribArray(0);
            glDisableVertexAttribArray(1);
        } else if (node->isSurface) {
            std::vector<unsigned short> indices = node->indices; //Triangles concaténés dans une liste
            std::vector <std::vector<unsigned short>> triangles = node->triangles;
            std::vector <glm::vec3> indexed_vertices = node->indexed_vertices;
            std::vector <glm::vec2> uvs = node->uvs;

            surfacePlane(indices, triangles, indexed_vertices, uvs, resolution, detailFactor);

            // Calculer les points min et max après transformation
            glm::vec4 temp_min_point = model_matrix * glm::vec4(indexed_vertices[0], 1.0f);
            glm::vec3 min_point = glm::vec3(temp_min_point);
            glm::vec3 max_point = min_point;
            for (const auto& vertex : indexed_vertices) {
                glm::vec4 temp_transformed_vertex = model_matrix * glm::vec4(vertex, 1.0f);
                glm::vec3 transformed_vertex = glm::vec3(temp_transformed_vertex);
                for (int i = 0; i < 3; ++i) {
                    min_point[i] = std::min(min_point[i], transformed_vertex[i]);
                    max_point[i] = std::max(max_point[i], transformed_vertex[i]);
                }
            }

            // Créer et assigner la nouvelle AABB
            node->aabb = AABB(min_point, max_point);

            // Load it into a VBO
            GLuint vertexbuffer;
            glGenBuffers(1, &vertexbuffer);
            glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
            glBufferData(GL_ARRAY_BUFFER, indexed_vertices.size() * sizeof(glm::vec3), &indexed_vertices[0],
                         GL_STATIC_DRAW);

            // Generate a buffer for the indices as well
            GLuint elementbuffer;
            glGenBuffers(1, &elementbuffer);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);

            GLuint uvbuffer;
            glGenBuffers(1, &uvbuffer);
            glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
            glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);

            glEnableVertexAttribArray(0);
            glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);

            glEnableVertexAttribArray(1);
            glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
            glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void *) 0);

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

            GLuint modelMatrixID = glGetUniformLocation(this->shader, "model");
            glUniformMatrix4fv(modelMatrixID, 1, GL_FALSE, &model_matrix[0][0]);

            glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_SHORT, (void *) 0);

            glDisableVertexAttribArray(0);
            glDisableVertexAttribArray(1);
        } else if (node->isCube) {
            std::vector<unsigned short> indices = node->indices;
            std::vector <std::vector<unsigned short>> triangles = node->triangles;
            std::vector <glm::vec3> indexed_vertices = node->indexed_vertices;
            std::vector <glm::vec2> uvs = node->uvs;

            newCube(indices, triangles, indexed_vertices, uvs, node->cubeSize, detailFactor, node->cubeProperties);

            // Load it into a VBO
            GLuint vertexbuffer;
            glGenBuffers(1, &vertexbuffer);
            glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
            glBufferData(GL_ARRAY_BUFFER, indexed_vertices.size() * sizeof(glm::vec3), &indexed_vertices[0],
                         GL_STATIC_DRAW);

            // Generate a buffer for the indices as well
            GLuint elementbuffer;
            glGenBuffers(1, &elementbuffer);
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);

            GLuint uvbuffer;
            glGenBuffers(1, &uvbuffer);
            glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
            glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);

            glEnableVertexAttribArray(0);
            glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
            glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void *) 0);

            glEnableVertexAttribArray(1);
            glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
            glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void *) 0);

            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

            GLuint modelMatrixID = glGetUniformLocation(this->shader, "model");
            glUniformMatrix4fv(modelMatrixID, 1, GL_FALSE, &model_matrix[0][0]);

            glDrawElements(GL_TRIANGLES, indices.size(), GL_UNSIGNED_SHORT, (void *) 0);

            glDisableVertexAttribArray(0);
            glDisableVertexAttribArray(1);
        }

        for (Node *child: node->children) {
            render(child, model_matrix);
        }

    }

private:
    void supprimerNodeDeParent(Node *parentNode, Node *nodeToRemove) {
        auto it = std::find(parentNode->children.begin(), parentNode->children.end(), nodeToRemove);
        if (it != parentNode->children.end()) {
            parentNode->children.erase(it);
            delete nodeToRemove;
        } else {
            for (Node *child: parentNode->children) {
                supprimerNodeDeParent(child, nodeToRemove);
            }
        }
    }
};


/**
 * fonction pour calculer le rebond du cube sur une surface
 * @param cube
 * @param normal normale de la surface
 */
void resolveCollision(Node &cube, glm::vec3 &normal, float restitution) {
    // Coefficient de restitution (élasticité) pour déterminer la vitesse de sortie.
    // 0 = inelastique, 1 = prfaitement élastique (pas de perte d'énergie cinétique)

    // Calculer la vitesse de sortie en utilisant la formule de réflexion
    cube.cubeProperties->velocity = cube.cubeProperties->velocity -
                                    (1 + restitution) * glm::dot(cube.cubeProperties->velocity, normal) * normal;
}

/**
 * fonction pour modifier la  position du cube en fonction de sa vitesse
 * cette fonction utilise la structure AABB
 * @param cube
 * @param deltaTime
 */
void updatePosCube(Node &cube, float deltaTime, std::vector<Node *> &platforms) {
    cube.cubeProperties->velocity += cube.cubeProperties->acceleration * deltaTime;
    glm::vec3 posCube = cube.transform.t + cube.cubeProperties->velocity * deltaTime;
    float half = cube.cubeSize / 2.0f;
    float mur_half_width = 5.0f; // Largeur du mur divisée par 2
    bool parTerre = posCube.y - half < 0.0f;

    cube.transform.t = posCube;
    cube.aabb.min = cube.transform.t - glm::vec3(cube.cubeSize / 2.0f);
    cube.aabb.max = cube.transform.t + glm::vec3(cube.cubeSize / 2.0f);


    for (Node *platform: platforms) {
        if (checkAABBCollision(cube.aabb, platform->aabb)) {
            if (platform->isSurface) {
                if (platform->surfaceOrientation == glm::vec3(0.0f, 1.0f, 0.0f)) {
                    posCube.y = platform->aabb.max.y + cube.cubeSize / 2.0f;
                    glm::vec3 normaleAuPlan(0.0f, 1.0f, 0.0f);
                    resolveCollision(cube, normaleAuPlan, 0.75f);
                    cube.transform.t = posCube;
                } else if (platform->surfaceOrientation == glm::vec3(1.0f, 0.0f, 0.0f)) {
                    posCube.x = platform->aabb.min.x - cube.cubeSize / 2.0f;
                    glm::vec3 normaleAuPlan(1.0f, 0.0f, 0.0f);
                    resolveCollision(cube, normaleAuPlan, 0.75f);
                    cube.transform.t = posCube;
                } else if (platform->surfaceOrientation == glm::vec3(-1.0f, 0.0f, 0.0f)) {
                    posCube.x = platform->aabb.max.x + cube.cubeSize / 2.0f;
                    glm::vec3 normaleAuPlan(-1.0f, 0.0f, 0.0f);
                    resolveCollision(cube, normaleAuPlan, 0.75f);
                    cube.transform.t = posCube;
                } else if (platform->surfaceOrientation == glm::vec3(0.0f, 0.0f, 1.0f)) {
                    posCube.z = platform->aabb.max.z + cube.cubeSize / 2.0f;
                    glm::vec3 normaleAuPlan(0.0f, 0.0f, 1.0f);
                    resolveCollision(cube, normaleAuPlan, 0.75f);
                    cube.transform.t = posCube;
                } else if (platform->surfaceOrientation == glm::vec3(0.0f, 0.0f, -1.0f)) {
                    posCube.z = platform->aabb.max.z + cube.cubeSize / 2.0f;
                    glm::vec3 normaleAuPlan(0.0f, 0.0f, -1.0f);
                    resolveCollision(cube, normaleAuPlan, 0.75f);
                    cube.transform.t = posCube;
                }
            }
        }
    }


    // Le coefficient de friction est compris entre 0 et 1,
    // où 0 signifie aucune friction et 1 signifie une friction infinie.
    if (parTerre) {
        float coef_f = 0.1;
        cube.cubeProperties->velocity.x *= (1 - coef_f);
        cube.cubeProperties->velocity.z *= (1 - coef_f);

        //ajout d'une borne inf pour stopper complètement le cube si vitesse trop faible
        float borne_inf = 0.001f;
        if (glm::length(cube.cubeProperties->velocity) < borne_inf) {
            cube.cubeProperties->velocity = glm::vec3(0.0f);
        }
    }

    cube.transform.t = posCube;

    //std::cout << "pos cube: " << cube.transform.t.x << ", " << cube.transform.t.y << ", " << cube.transform.t.z
    //          << std::endl;
}

/**
 * fonction test pour vérifier la rotation des nodes bras
 * @param bras1
 * @param bras2
 * @param arm_rotation
 */
void updateArmRotation(Node *bras1, Node *bras2, float arm_rotation) {
    glm::mat3x3 rotationMatrix = glm::mat3x3(glm::rotate(glm::mat4(1.0f), arm_rotation, glm::vec3(0.0f, 1.0f, 0.0f)));
    bras1->transform.r = rotationMatrix;
    bras2->transform.r = rotationMatrix;
}

void faceAFace(Node &body, Node &pouleBody) {
    glm::vec3 direction = glm::normalize(body.transform.t - pouleBody.transform.t);
    float angleBody = atan2(direction.x, direction.z);
    float anglePoule = angleBody + glm::pi<float>();

    float angleSupplementaire = glm::pi<float>(); // Ajustez l'angle supplémentaire pour que le personnage fasse face à la poule avec la face avant de son corps
    body.transform.r = glm::mat3x3(glm::rotate(glm::mat4(1.0f), angleBody + angleSupplementaire + glm::pi<float>(),
                                               glm::vec3(0.0f, 1.0f, 0.0f)));
    pouleBody.transform.r = glm::mat3x3(glm::rotate(glm::mat4(1.0f), anglePoule, glm::vec3(0.0f, 1.0f, 0.0f)));
}

CubeProperties *cubeProps = new CubeProperties(glm::vec3(0.0f, 0.0f, 0.0f), 0.015f, glm::vec3(0.0f, 0.0f, 0.0f));

// Création des pattes de la poule
Node *poulePatte1 = new Node(
        Transform(glm::vec3(0.25f, 0.8f, 0.25f),
                  glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                  glm::vec3(-0.01f, -0.07f, 0.0f)), false, false, true, 0.05f, cubeProps);

Node *poulePatte2 = new Node(
        Transform(glm::vec3(0.25f, 0.8f, 0.25f),
                  glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                  glm::vec3(0.01f, -0.07f, 0.0f)), false, false, true, 0.05f, cubeProps);


CubeProperties *pouleBodyProps = new CubeProperties(glm::vec3(0.0f, 0.0f, 0.0f), 0.2f, glm::vec3(0.0f, 0.0f, 0.0f));
Node* pouleBody2 = new Node(
        Transform(glm::vec3(1.0f, 1.0f, 1.0f),
                  glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                  glm::vec3(1.0f, 0.2f, 0.0f)), false, false, true, 0.1f, pouleBodyProps);

/**
 * création de la minipoule à la fin du combat de boss
 * @param grapheScene
 */
void creerPoule(GrapheScene grapheScene){

    pouleBody2->active = true;

    Node *pouleTete2 = new Node(
            Transform(glm::vec3(1.0f, 1.0f, 1.0f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.0f, 0.05f, -0.07f)), false, false, true, 0.05f, cubeProps);

    Node *pouleBec2 = new Node(
            Transform(glm::vec3(0.5f, 0.5f, 1.2f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.0f, 0.0f, -0.035f)), false, false, true, 0.02f, cubeProps);

    Node *pouleCrete2 = new Node(
            Transform(glm::vec3(0.2f, 0.65f, 0.4f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.0f, 0.02f, 0.0f)), false, false, true, 0.05f, cubeProps);

    Node *pouleAile12 = new Node(
            Transform(glm::vec3(0.3f, 0.8f, 0.8f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(-0.07f, 0.0f, 0.0f)), false, false, true, 0.08f, cubeProps);

    Node *pouleAile22 = new Node(
            Transform(glm::vec3(0.3f, 0.8f, 0.8f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.07f, 0.0f, 0.0f)), false, false, true, 0.08f, cubeProps);

    Node *poulePatte12 = new Node(
            Transform(glm::vec3(0.25f, 0.8f, 0.25f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(-0.01f, -0.07f, 0.0f)), false, false, true, 0.05f, cubeProps);

    Node *poulePatte22 = new Node(
            Transform(glm::vec3(0.25f, 0.8f, 0.25f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.01f, -0.07f, 0.0f)), false, false, true, 0.05f, cubeProps);

    grapheScene.ajoutNode(pouleBody2);
    grapheScene.ajoutNodeAParent(pouleBody2, pouleTete2);
    grapheScene.ajoutNodeAParent(pouleTete2, pouleBec2);
    grapheScene.ajoutNodeAParent(pouleTete2, pouleCrete2);
    grapheScene.ajoutNodeAParent(pouleBody2, pouleAile12);
    grapheScene.ajoutNodeAParent(pouleBody2, pouleAile22);
    grapheScene.ajoutNodeAParent(pouleBody2, poulePatte12);
    grapheScene.ajoutNodeAParent(pouleBody2, poulePatte22);

    /*std::vector<Node*> pouleParts = {pouleBody2, pouleTete, pouleBec, pouleAile1, pouleAile2, poulePatte1, poulePatte2, pouleCrete};
    grapheScene.pouleParts.assign(pouleParts.begin(), pouleParts.end());*/
    grapheScene.pouleParts.clear();
    std::vector<Node*> pouleParts = {pouleBody2, pouleTete2, pouleBec2, pouleAile12, pouleAile22, poulePatte12, poulePatte22, pouleCrete2};
    for(auto part : pouleParts) {
        grapheScene.pouleParts.push_back(part);
    }

}

bool pouleAuSol(const Node *pouleBody) {
    Transform identityTransform(glm::vec3(1.0f), glm::mat3x3(1.0f), glm::vec3(0.0f));

    // Parcourir les enfants de pouleBody pour trouver les pattes
    for (const Node *child : pouleBody->children) {
        if (child == poulePatte1 || child == poulePatte2) {
            // Obtenir la transformation absolue de la patte
            Transform absolutePatteTransform = getAbsoluteTransform(const_cast<Node *>(pouleBody), const_cast<Node *>(child), identityTransform);
            float patteHeight = 0.4f;

            // Vérifier si la patte est au sol
            if ((absolutePatteTransform.t.y - child->cubeSize / 2.0f - patteHeight) < 0.0f) {
                return true;
            }
        }
    }

    return false;
}

void movePoule(Node &pouleBody, const Node &playerBody, bool pouleAuSol) {
    std::cout << "poulebody x: " << pouleBody.transform.t.x << ", y: " << pouleBody.transform.t.y << ", z: " << pouleBody.transform.t.z
              << ", pouleAuSol?: " << pouleAuSol << ", HP:" << HpPoule << std::endl;
    if (pouleAuSol) {
        // Utiliser les mêmes paramètres de vitesse et d'accélération que dans moveBody
        float v0 = 0.5f; // vitesse initiale
        float poids = pouleBody.cubeProperties->weight;

        // Calculer la direction du saut en fonction de la position du joueur
        glm::vec3 jumpDirection = glm::normalize(playerBody.transform.t - pouleBody.transform.t);
        jumpDirection.y = 0.0f; // Ignorer la composante y pour le moment
        pouleBody.cubeProperties->velocity = v0 * jumpDirection;

        // Ajouter une force vers le haut pour simuler un saut
        pouleBody.cubeProperties->velocity.y += v0;

        // Mettre à jour le poids de la poule et l'accélération due à la gravité
        pouleBody.cubeProperties->weight = poids;
        pouleBody.cubeProperties->acceleration = gravity * poids;
    }
}

/**
 * fonction de reposition du body
 * @param body
 * @param deltaTime
 * @param platforms
 */
void updatePosBody(Node &body, Node &pouleBody, float deltaTime, std::vector<Node *> &platforms) {

    if(pouleBody2->active){
        if(checkAABBCollision(body.aabb,pouleBody2->aabb)){
            body.transform.t.y += 5.0f * deltaTime;
            body.aabb.min = body.transform.t - glm::vec3(body.cubeSize / 2.0f) * body.transform.s;
            body.aabb.max = body.transform.t + glm::vec3(body.cubeSize / 2.0f) * body.transform.s;
            pouleBody2->transform.t.y = body.transform.t.y;
            pouleBody2->aabb.min = body.transform.t - glm::vec3(body.cubeSize / 2.0f) * body.transform.s;
            pouleBody2->aabb.max = body.transform.t + glm::vec3(body.cubeSize / 2.0f) * body.transform.s;
            camera_target = body.transform.t;
            camera_position = glm::vec3(0.0f, 1.0f, 3.0f);
        }
    }

    body.cubeProperties->velocity += body.cubeProperties->acceleration * deltaTime;
    glm::vec3 posBody = body.transform.t + body.cubeProperties->velocity * deltaTime;
    float half = body.cubeSize / 2.0f;

    // Prendre en compte la hauteur des jambes pour le contact avec le sol
    float legHeight = 0.08f;
    bool parTerre = posBody.y - half - legHeight < 0.0f;

    pouleBody.cubeProperties->velocity += pouleBody.cubeProperties->acceleration * deltaTime;
    glm::vec3 posPoule = pouleBody.transform.t + pouleBody.cubeProperties->velocity * deltaTime;
    pouleBody.transform.t = posPoule;

    // Créer une variable pour la hauteur des pattes de la poule
    float patteHeight = 0.4f;

    // Vérifiez si la poule est au sol et ajustez sa position en conséquence
    bool pouleOnPlatform = false;
    for (Node *platform: platforms) {
        if (checkAABBCollision(pouleBody.aabb, platform->aabb)) {
            posPoule.y = platform->aabb.max.y + pouleBody.cubeSize / 2.0f + patteHeight;
            pouleBody.cubeProperties->velocity.y = 0.0f;
            pouleOnPlatform = true;
            break;
        }
    }
    if (!pouleOnPlatform && pouleAuSol(&pouleBody)) {
        pouleBody.cubeProperties->velocity.y = 0.0f;
        posPoule.y = pouleBody.cubeSize / 2.0f + patteHeight;
    }
    pouleBody.transform.t = posPoule;

    // Appeler movePoule en passant le paramètre pouleAuSol
    movePoule(pouleBody, body, !pouleOnPlatform && pouleAuSol(&pouleBody));

    if (parTerre) {
        body.cubeProperties->velocity.y = 0.0f;
        float coef_f = 0.1;
        body.cubeProperties->velocity.x *= (1 - coef_f);
        body.cubeProperties->velocity.z *= (1 - coef_f);

        // Ajout d'une borne inf pour stopper complètement le personnage si vitesse trop faible
        float borne_inf = 0.01f;
        if (glm::length(body.cubeProperties->velocity) < borne_inf) {
            body.cubeProperties->velocity = glm::vec3(0.0f);
        }

        // Ajuster la position y de body pour qu'elle repose sur le sol
        posBody.y = half + legHeight;
    }

    body.transform.t = posBody;
    body.aabb.min = body.transform.t - glm::vec3(body.cubeSize / 2.0f) * body.transform.s;
    body.aabb.max = body.transform.t + glm::vec3(body.cubeSize / 2.0f) * body.transform.s;

    for (Node *platform: platforms) {
        if (checkAABBCollision(body.aabb, platform->aabb)) {
            if (platform->isSurface) {
                if (platform->surfaceOrientation == glm::vec3(0.0f, 1.0f, 0.0f)) {
                    posBody.y = platform->aabb.max.y + body.cubeSize / 2.0f + legHeight;
                    glm::vec3 normaleAuPlan(0.0f, 1.0f, 0.0f);
                    resolveCollision(body, normaleAuPlan, 0.0f);

                } else if (platform->surfaceOrientation == glm::vec3(1.0f, 0.0f, 0.0f)) {
                    posBody.x = platform->aabb.min.x - body.cubeSize / 2.0f;
                    glm::vec3 normaleAuPlan(1.0f, 0.0f, 0.0f);
                    resolveCollision(body, normaleAuPlan, 0.0f);
                    body.transform.t = posBody;

                } else if (platform->surfaceOrientation == glm::vec3(-1.0f, 0.0f, 0.0f)) {
                    posBody.x = platform->aabb.max.x + body.cubeSize / 2.0f;
                    glm::vec3 normaleAuPlan(-1.0f, 0.0f, 0.0f);
                    resolveCollision(body, normaleAuPlan, 0.0f);
                    body.transform.t = posBody;

                } else if (platform->surfaceOrientation == glm::vec3(0.0f, 0.0f, 1.0f)) {
                    posBody.z = platform->aabb.min.z - body.cubeSize / 2.0f;
                    glm::vec3 normaleAuPlan(0.0f, 0.0f, 1.0f);
                    resolveCollision(body, normaleAuPlan, 0.0f);
                    body.transform.t = posBody;

                } else if (platform->surfaceOrientation == glm::vec3(0.0f, 0.0f, -1.0f)) {
                    posBody.z = platform->aabb.max.z + body.cubeSize / 2.0f;
                    glm::vec3 normaleAuPlan(0.0f, 0.0f, -1.0f);
                    resolveCollision(body, normaleAuPlan, 0.0f);
                    body.transform.t = posBody;
                }
            }
        }
    }
    body.transform.t = posBody;
    pouleBody.transform.t = posPoule;
    pouleBody.aabb.min = pouleBody.transform.t - glm::vec3(pouleBody.cubeSize / 2.0f) * pouleBody.transform.s;
    pouleBody.aabb.max = pouleBody.transform.t + glm::vec3(pouleBody.cubeSize / 2.0f) * pouleBody.transform.s;

    faceAFace(body, pouleBody);
}

float verticalAng, horizontalAng;
void updateCameraPos(Transform player) {
    camera_target = player.t;
    //
    if(horizontalAng && verticalAng) {
        // Get mouse position
        double xpos, ypos;
        float mouseSpeed = 0.003f;
        glfwGetCursorPos(window, &xpos, &ypos);
        // reset mouse for next frame
        glfwSetCursorPos(window, 1024 / 2, 768 / 2);

        horizontalAng += mouseSpeed * float(1024 / 2 - xpos);
        verticalAng += mouseSpeed * float(768 / 2 - ypos);
        verticalAng = std::clamp(verticalAng, -0.3f, 0.01f);

        glm::vec3 direction(
                cos(verticalAng) * sin(horizontalAng),
                sin(verticalAng),
                cos(verticalAng) * cos(horizontalAng)
        );

        glm::vec3 final_camera_pos = camera_target - direction * 1.0f;
        camera_position = final_camera_pos;
    } else {
        camera_position = player.t + glm::vec3(0, 0.1f, 0.25f);
        glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
        // Compute new orientation
        glm::vec3 direction = glm::normalize(camera_target - camera_position);
        glm::vec3 right = glm::normalize(glm::cross(camera_up, direction));
        verticalAng = asin(direction.y);
        horizontalAng = acos(right.z) + 3.14f / 2.0f;
    }
}

void moveBody(Node &body, GLFWwindow *window);

void jeterCube(Transform &bras1Transform, glm::vec3 upperArmEnd1, GrapheScene &grapheScene, Node *poule, GLFWwindow *window);

//vecteur de cubes lancés
std::vector<Node *> cubesLances;

void poulePrendDegats(Node *cube, Node *poule, GrapheScene &grapheScene, int damage) {
    if (checkAABBCollision(cube->aabb, poule->aabb)) {
        HpPoule -= damage;
        if (HpPoule <= 0) {
            //HpPoule = 0;
            grapheScene.supprimerNode(poule);
            creerPoule(grapheScene);
        }
        // Trouvez le cube dans le vecteur cubesLances et supprimez-le
        auto it = std::find(cubesLances.begin(), cubesLances.end(), cube);
        if (it != cubesLances.end()) {
            grapheScene.supprimerNode(*it);
            cubesLances.erase(it);
        }
    }
}


// création de la node root, de la surface plane et du mur
Node root(
        Transform(glm::vec3(1.0f), glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                  glm::vec3(0.0f, 0.0f, 0.0f)), false, false, false);
Node *surface = new Node(
        Transform(glm::vec3(1.0f),
                  glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))) *
                  glm::mat3x3(glm::scale(glm::mat4(1.0f), glm::vec3(5.0f, 1.0f, 5.0f))),
                  glm::vec3(-1.0f, 0.0f, -4.0f)),
        false, true, false, glm::vec3(0.0f, 1.0f, 0.0f));

glm::mat4 wall_rotation = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(1.0f, 0.0f, 0.0f));
glm::mat4 wall_scale = glm::scale(glm::mat4(1.0f), glm::vec3(5.0f, 1.0f, 5.0f));
glm::mat4 wall_transform = wall_rotation * wall_scale;
Node *mur = new Node(
        Transform(glm::vec3(1.0f), glm::mat3x3(wall_transform), glm::vec3(-1.0f, 4.0f, -4.0f)),
        false, true, false, glm::vec3(0.0f, 0.0f, -1.0f));


int main(void) {
    // Initialise GLFW
    if (!glfwInit()) {
        fprintf(stderr, "Failed to initialize GLFW\n");
        getchar();
        return -1;
    }

    glfwWindowHint(GLFW_SAMPLES, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    // Open a window and create its OpenGL context
    window = glfwCreateWindow(1024, 768, "Moteur de jeu", NULL, NULL);
    if (window == NULL) {
        fprintf(stderr,
                "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n");
        getchar();
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);

    // Initialize GLEW
    glewExperimental = true; // Needed for core profile
    if (glewInit() != GLEW_OK) {
        fprintf(stderr, "Failed to initialize GLEW\n");
        getchar();
        glfwTerminate();
        return -1;
    }

    // Ensure we can capture the escape key being pressed below
    glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
    // Hide the mouse and enable unlimited mouvement
    //  glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    // Set the mouse at the center of the screen
    glfwPollEvents();
    glfwSetCursorPos(window, 1024 / 2, 768 / 2);

    // Dark blue background
    glClearColor(0.8f, 0.8f, 0.8f, 0.0f);

    // Enable depth test
    glEnable(GL_DEPTH_TEST);
    // Accept fragment if it closer to the camera than the former one
    glDepthFunc(GL_LESS);

    // Cull triangles which normal is not towards the camera
    //glEnable(GL_CULL_FACE);

    GLuint VertexArrayID;
    glGenVertexArrays(1, &VertexArrayID);
    glBindVertexArray(VertexArrayID);

    // Create and compile our GLSL program from the shaders
    GLuint programID = LoadShaders("vertex_shader.glsl", "fragment_shader.glsl");

    // Load it into a VBO
    GLuint vertexbuffer;
    glGenBuffers(1, &vertexbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
    glBufferData(GL_ARRAY_BUFFER, indexed_vertices.size() * sizeof(glm::vec3), &indexed_vertices[0], GL_STATIC_DRAW);

    // Generate a buffer for the indices as well
    GLuint elementbuffer;
    glGenBuffers(1, &elementbuffer);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);

    GLuint uvbuffer;
    glGenBuffers(1, &uvbuffer);
    glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
    glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);

    // Get a handle for our "LightPosition" uniform
    glUseProgram(programID);

    GLuint LightID = glGetUniformLocation(programID, "LightPosition_worldspace");
    // Get texture
    GLuint Texture = loadBMP_custom("godziText.bmp");
    int width, height, channels;
    unsigned char *grassData = stbi_load("grass.png", &width, &height, &channels, STBI_rgb);
    unsigned char *rockData = stbi_load("rock.png", &width, &height, &channels, STBI_rgb);
    unsigned char *snowData = stbi_load("snow.png", &width, &height, &channels, STBI_rgb);
    unsigned int grassTexture, rockTexture, snowTexture;
    glGenTextures(1, &grassTexture);
    glGenTextures(1, &rockTexture);
    glGenTextures(1, &snowTexture);
    //Get sampler
    GLuint TextureSampler = glGetUniformLocation(programID, "colorTexture");
    GLuint grassSampler = glGetUniformLocation(programID, "grass");
    GLuint rocksSampler = glGetUniformLocation(programID, "rock");
    GLuint snowsSampler = glGetUniformLocation(programID, "snow");
    //Link texture to sampler
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, Texture);
    glUniform1i(TextureSampler, 0);

    //grass
    glActiveTexture(GL_TEXTURE1);
    glBindTexture(GL_TEXTURE_2D, grassTexture); // Lier la texture à l'identifiant
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); // Configurer la répétition horizontale
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); // Configurer la répétition verticale
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                    GL_LINEAR); // Configurer le filtrage de la texture pour les réductions
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                    GL_LINEAR); // Configurer le filtrage de la texture pour les agrandissements
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE,
                 grassData); // Envoyer les données de la texture à la carte graphique
    glGenerateMipmap(GL_TEXTURE_2D);
    glUniform1i(grassSampler, 1);

    //rock
    glActiveTexture(GL_TEXTURE2);
    glBindTexture(GL_TEXTURE_2D, rockTexture); // Lier la texture à l'identifiant
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); // Configurer la répétition horizontale
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); // Configurer la répétition verticale
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                    GL_LINEAR); // Configurer le filtrage de la texture pour les réductions
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                    GL_LINEAR); // Configurer le filtrage de la texture pour les agrandissements
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE,
                 rockData); // Envoyer les données de la texture à la carte graphique
    glGenerateMipmap(GL_TEXTURE_2D);
    glUniform1i(rocksSampler, 2);

    //snow
    glActiveTexture(GL_TEXTURE3);
    glBindTexture(GL_TEXTURE_2D, snowTexture); // Lier la texture à l'identifiant
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT); // Configurer la répétition horizontale
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT); // Configurer la répétition verticale
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,
                    GL_LINEAR); // Configurer le filtrage de la texture pour les réductions
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,
                    GL_LINEAR); // Configurer le filtrage de la texture pour les agrandissements
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE,
                 snowData); // Envoyer les données de la texture à la carte graphique
    glGenerateMipmap(GL_TEXTURE_2D);
    glUniform1i(snowsSampler, 3);

    // For speed computation
    double lastTime = glfwGetTime();
    int nbFrames = 0;
    //glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    //Graphe de scène
    GrapheScene grapheScene(&root, programID);
    grapheScene.ajoutPlateforme(surface);
    grapheScene.ajoutPlateforme(mur);


    //création du bonhomme dans le graphe de scène
    CubeProperties *bodyProps = new CubeProperties(glm::vec3(0.0f, 0.0f, 0.0f), 0.2f, glm::vec3(0.0f, 0.0f, 0.0f));

    Node *body = new Node(
            Transform(glm::vec3(0.5f, 1.0f, 0.5f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.0f, 0.2f, 0.0f)), false, false, true, 0.1f, bodyProps);

    Node *tete = new Node(
            Transform(glm::vec3(2.0f, 1.0f, 2.0f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.0f, 0.1f, 0.0f)), true, false, false, 0.05f, cubeProps);

    glm::vec3 bodyPosition = glm::vec3(0.0f, 0.2f, 0.0f);

    Node *jambe1 = new Node(
            Transform(glm::vec3(0.25f, 0.8f, 0.25f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.01f, -0.08f, 0.0f), glm::vec3(0.04f, -0.04f, 0.0f)), false, false, true, 0.1f,
            cubeProps);

    Node *jambe2 = new Node(
            Transform(glm::vec3(0.25f, 0.8f, 0.25f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(-0.01f, -0.08f, 0.0f), glm::vec3(-0.04f, -0.04f, 0.0f)), false, false, true, 0.1f,
            cubeProps);

    glm::vec3 upperArmEnd1 = glm::vec3(0.065f, 0.04f, 0.0f);
    glm::vec3 upperArmEnd2 = glm::vec3(-0.065f, 0.04f, 0.0f);
    Node *bras1 = new Node(
            Transform(glm::vec3(0.25f, 0.8f, 0.25f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), arm_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.015f, 0.0f, 0.0f), upperArmEnd1), false, false, true, 0.1f, cubeProps);

    Node *bras2 = new Node(
            Transform(glm::vec3(0.25f, 0.8f, 0.25f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), arm_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(-0.015f, 0.0f, 0.0f), upperArmEnd2), false, false, true, 0.1f, cubeProps);


    // Créez des nœuds de sphère pour les centres de rotation du body
    Node *centerRotBras1 = new Node(
            Transform(glm::vec3(2.0f, 1.0f, 2.0f), glm::mat3x3(1.0f), bras1->transform.rotationCenter),
            true, false, false, 0.02f);

    Node *centerRotBras2 = new Node(
            Transform(glm::vec3(2.0f, 1.0f, 2.0f), glm::mat3x3(1.0f), bras2->transform.rotationCenter),
            true, false, false, 0.02f);

    Node *centerRotJambe1 = new Node(
            Transform(glm::vec3(2.0f, 1.0f, 2.0f), glm::mat3x3(1.0f), jambe1->transform.rotationCenter),
            true, false, false, 0.02f);

    Node *centerRotJambe2 = new Node(
            Transform(glm::vec3(2.0f, 1.0f, 2.0f), glm::mat3x3(1.0f), jambe2->transform.rotationCenter),
            true, false, false, 0.02f);

    // Création du corps de la poule dans le graphe de scène
    //CubeProperties *pouleBodyProps = new CubeProperties(glm::vec3(0.0f, 0.0f, 0.0f), 0.2f, glm::vec3(0.0f, 0.0f, 0.0f));

    pouleBody2->active = false;

    Node *pouleBody = new Node(
            Transform(glm::vec3(5.0f, 5.0f, 5.0f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(1.0f, 0.45f, 0.0f)), false, false, true, 0.1f, pouleBodyProps);

    Node *pouleTete = new Node(
            Transform(glm::vec3(1.0f, 1.0f, 1.0f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.0f, 0.05f, -0.07f)), false, false, true, 0.05f, cubeProps);

    Node *pouleBec = new Node(
            Transform(glm::vec3(0.5f, 0.5f, 1.2f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.0f, 0.0f, -0.035f)), false, false, true, 0.02f, cubeProps);

    Node *pouleCrete = new Node(
            Transform(glm::vec3(0.2f, 0.65f, 0.4f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.0f, 0.02f, 0.0f)), false, false, true, 0.05f, cubeProps);

    Node *pouleAile1 = new Node(
            Transform(glm::vec3(0.3f, 0.8f, 0.8f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(-0.07f, 0.0f, 0.0f)), false, false, true, 0.08f, cubeProps);

    Node *pouleAile2 = new Node(
            Transform(glm::vec3(0.3f, 0.8f, 0.8f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.07f, 0.0f, 0.0f)), false, false, true, 0.08f, cubeProps);

    grapheScene.ajoutNode(body);
    grapheScene.ajoutNodeAParent(body, tete);
    grapheScene.ajoutNodeAParent(body, jambe1);
    grapheScene.ajoutNodeAParent(body, jambe2);
    grapheScene.ajoutNodeAParent(body, bras1);
    grapheScene.ajoutNodeAParent(body, bras2);

    // Ajout des nœuds centre de rotation à la scène
    grapheScene.ajoutNodeAParent(body, centerRotBras1);
    grapheScene.ajoutNodeAParent(body, centerRotBras2);
    grapheScene.ajoutNodeAParent(body, centerRotJambe1);
    grapheScene.ajoutNodeAParent(body, centerRotJambe2);

    // Ajout des éléments de la poule au graphe de scène
    grapheScene.ajoutNode(pouleBody);
    grapheScene.ajoutNodeAParent(pouleBody, pouleTete);
    grapheScene.ajoutNodeAParent(pouleTete, pouleBec);
    grapheScene.ajoutNodeAParent(pouleTete, pouleCrete);
    grapheScene.ajoutNodeAParent(pouleBody, pouleAile1);
    grapheScene.ajoutNodeAParent(pouleBody, pouleAile2);
    grapheScene.ajoutNodeAParent(pouleBody, poulePatte1);
    grapheScene.ajoutNodeAParent(pouleBody, poulePatte2);

    //grapheScene.pouleParts = {pouleBody, pouleTete, pouleBec, pouleAile1, pouleAile2, poulePatte1, poulePatte2};
    std::vector<Node*> pouleParts = {pouleBody, pouleTete, pouleBec, pouleAile1, pouleAile2, poulePatte1, poulePatte2, pouleCrete};
    grapheScene.pouleParts.assign(pouleParts.begin(), pouleParts.end());

    // Création de la barre de vie de la poule
    CubeProperties *barreVieProps = new CubeProperties(glm::vec3(0.0f, 0.0f, 0.0f), 0.2f, glm::vec3(0.0f, 0.0f, 0.0f));

    Node *barreVie = new Node(
            Transform(glm::vec3(0.6f, 0.1f, 0.1f),
                      glm::mat3x3(glm::rotate(glm::mat4(1.0f), sun_rotation, glm::vec3(0.0f, 1.0f, 0.0f))),
                      glm::vec3(0.0f, 0.01f, -0.1f)),
            false, false, true, 0.1f, barreVieProps);

    grapheScene.ajoutNodeAParent(pouleBody, barreVie);
    grapheScene.barreVie = barreVie;

    glm::mat3x3 initialArmRotation = bras1->transform.r;
    do {
        // Measure speed
        // per-frame time logic
        // --------------------
        float currentFrame = glfwGetTime();
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        // input
        // -----
        processInput(window);
        moveBody(*body, window);
        updatePosBody(*body, *pouleBody, deltaTime, grapheScene.platforms);

        //animation simple (non fonctionnel)
        if (updateRotations) {
            glm::mat4 rotationMat4 = glm::rotate(glm::mat4(1.0f), glm::radians(90.0f), glm::vec3(1, 0, 0));
            glm::mat3x3 rotation = glm::mat3(rotationMat4);
            bras1->transform.r = initialArmRotation * rotation;
            updateRotations = false;
        }

        Transform identityTransform(glm::vec3(1.0f), glm::mat3x3(1.0f), glm::vec3(0.0f));
        Transform absoluteBras1Transform = getAbsoluteTransform(body, bras1, identityTransform);
        jeterCube(absoluteBras1Transform, upperArmEnd1, grapheScene, pouleBody, window);
        //std::cout << "Bras global position: " << absoluteBras1Transform.t.x << ", " << absoluteBras1Transform.t.y << ", " << absoluteBras1Transform.t.z << std::endl;

        // Clear the screen
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        // Use our shader
        glUseProgram(programID);

        // Load it into a VBO
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
        glBufferData(GL_ARRAY_BUFFER, indexed_vertices.size() * sizeof(glm::vec3), &indexed_vertices[0],
                     GL_STATIC_DRAW);

        // Generate a buffer for the indices as well
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_STATIC_DRAW);

        glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
        glBufferData(GL_ARRAY_BUFFER, uvs.size() * sizeof(glm::vec2), &uvs[0], GL_STATIC_DRAW);


        /*****************TODO***********************/

        glm::mat4 model_Matrix;
        model_Matrix = glm::mat4(1.0f);
        glm::mat4 ViewMatrix;
        ViewMatrix = mat4(1.0f);
        ViewMatrix = glm::lookAt(camera_position, camera_target, camera_up);
        glm::mat4 ProjectionMatrix;
        ProjectionMatrix = mat4(1.0f);
        ProjectionMatrix = glm::perspective(glm::radians(45.0f), 4.0f / 3.0f, 0.1f, 100.0f);

        // Update arm rotation
        updateArmRotation(bras1, bras2, arm_rotation);

        //position du cube en fonction de sa vitesse
        for (Node *cubeLance: cubesLances) {
            updatePosCube(*cubeLance, deltaTime, grapheScene.platforms);
        }

        //camera_target = body->transform.t + glm::vec3(0.0f, 0.0f, -1.0f);
        updateCameraPos(body->transform);


        //affichage node
        //affichage(&root, model_Matrix, programID);
        grapheScene.update(ViewMatrix, ProjectionMatrix);
        grapheScene.render(grapheScene.root, model_Matrix);

        //GLuint ModelMatrixID = glGetUniformLocation(programID, "model");
        GLuint ViewMatrixID = glGetUniformLocation(programID, "view");
        GLuint ProjectionMatrixID = glGetUniformLocation(programID, "projection");

        //glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &model_Matrix[0][0]);
        glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &ViewMatrix[0][0]);
        glUniformMatrix4fv(ProjectionMatrixID, 1, GL_FALSE, &ProjectionMatrix[0][0]);

        /****************************************/

        // 1rst attribute buffer : vertices
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
        glVertexAttribPointer(
                0,                  // attribute
                3,                  // size
                GL_FLOAT,           // type
                GL_FALSE,           // normalized?
                0,                  // stride
                (void *) 0            // array buffer offset
        );

        // Index buffer
        //2nd attribute buffer : UVs
        glEnableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, uvbuffer);
        glVertexAttribPointer(
                1,                                // attribute
                2,                                // size
                GL_FLOAT,                         // type
                GL_FALSE,                         // normalized?
                0,                                // stride
                (void *) 0                          // array buffer offset
        );
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

        glDrawElements(
                GL_TRIANGLES,      // mode
                indices.size(),    // count
                GL_UNSIGNED_SHORT,   // type
                (void *) 0           // element array buffer offset
        );

        glDisableVertexAttribArray(0);

        // Swap buffers
        glfwSwapBuffers(window);
        glfwPollEvents();


    } // Check if the ESC key was pressed or the window was closed
    while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
           glfwWindowShouldClose(window) == 0);

    // Cleanup VBO and shader
    glDeleteBuffers(1, &vertexbuffer);
    glDeleteBuffers(1, &uvbuffer);
    glDeleteBuffers(1, &elementbuffer);
    glDeleteProgram(programID);
    glDeleteVertexArrays(1, &VertexArrayID);

    // Close OpenGL window and terminate GLFW
    glfwTerminate();

    return 0;
}

// process all input: query GLFW whether relevant keys are pressed/released this frame and react accordingly
// ---------------------------------------------------------------------------------------------------------
void processInput(GLFWwindow *window) {
    static bool firstMouse = true;
    if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);

    //Camera zoom in and out
    float cameraSpeed = 2.5 * deltaTime;


    if (glfwGetKey(window, GLFW_KEY_Q) == GLFW_PRESS)
        camera_position += cameraSpeed * camera_target;
    if (glfwGetKey(window, GLFW_KEY_E) == GLFW_PRESS)
        camera_position -= cameraSpeed * camera_target;

    if (glfwGetKey(window, GLFW_KEY_P) == GLFW_PRESS)
        arm_rotation += 0.1f;
    if (glfwGetKey(window, GLFW_KEY_O) == GLFW_PRESS)
        arm_rotation -= 0.1f;

    //translations
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        camera_position += cameraSpeed * camera_up;
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        camera_position -= cameraSpeed * camera_up;
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
        //camera_position -= cameraSpeed * glm::normalize(glm::cross(camera_target, camera_up));
        glm::vec3 camera_right = glm::normalize(glm::cross(camera_target - camera_position, camera_up));
        camera_position -= camera_right * cameraSpeed;
    }
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
        //camera_position += cameraSpeed * glm::normalize(glm::cross(camera_target, camera_up));
        glm::vec3 camera_right = glm::normalize(glm::cross(camera_target - camera_position, camera_up));
        camera_position += camera_right * cameraSpeed;
    }

    //resolution
    if (glfwGetKey(window, GLFW_KEY_KP_ADD) == GLFW_PRESS) {
        resolution += 1.0f;
        //surfacePlane(indices, triangles, indexed_vertices, uvs, resolution);
        std::cout << "Résolution: " << resolution << std::endl;
    }
    if (glfwGetKey(window, GLFW_KEY_KP_SUBTRACT) == GLFW_PRESS) {
        resolution -= 1.0f;
        if (resolution < 1.0f) {
            resolution = 1.0f;
        }
        std::cout << "Résolution: " << resolution << std::endl;
        //surfacePlane(indices, triangles, indexed_vertices, uvs, resolution);
    }

}


float lerpRotation(float x, float y, float t) {

    x = x - ((int)(x/2/3.14f)) * (2 * 3.14f);
    y = y - ((int)(y/2/3.14f)) * (2 * 3.14f);

    if(x - y > 3.14) {
        y += 3.14 * 2;
    } else if(x - y < -3.14) {
        x += 3.14 * 2;
    }

    return x * (1.0 - t) + y * t;
}
float targetRotation, currentRotation;

/**
 * fonction permettant un déplacement simple du body
 * @param body
 * @param window
 */
void moveBody(Node &body, GLFWwindow *window) {
    float bodySpeed = 1.0f * deltaTime;
    glm::vec3 inputDirection = glm::vec3(0,0,0);
    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS) {
        inputDirection.z -= 1;
    }
    if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS) {
        inputDirection.z += 1;
    }
    if (glfwGetKey(window, GLFW_KEY_LEFT) == GLFW_PRESS) {
        inputDirection.x -= 1;
    }
    if (glfwGetKey(window, GLFW_KEY_RIGHT) == GLFW_PRESS) {

        inputDirection.x += 1;
    }
    float rotationY = horizontalAng - 3.14f;
    glm::vec3 direction(
            cos(0) * sin(rotationY),
            sin(0),
            cos(0) * cos(rotationY));

    // Right vector
    glm::vec3 right = glm::vec3(
            sin(rotationY - 3.14 / 2.0f),
            0,
            cos(rotationY - 3.14 / 2.0f));

    body.transform.t += (-inputDirection.x * right + inputDirection.z * direction) * bodySpeed;
    if(!targetRotation) {
        targetRotation = rotationY;
        currentRotation = rotationY;
    }
    if(inputDirection.x != 0 && inputDirection.z == 0) {
        targetRotation = rotationY - inputDirection.x * 3.14 / 2;
    } else if(inputDirection.x == 0 && inputDirection.z != 0) {
        targetRotation = rotationY + std::clamp(inputDirection.z, 0.0f, 1.0f) * 3.14f;
    } else if(inputDirection.x == 1 && inputDirection.z == 1) {
        targetRotation = rotationY + 3.14 / 4;
    } else if(inputDirection.x == -1 && inputDirection.z == 1) {
        targetRotation = rotationY + 7 * 3.14 / 4;
    } else if(inputDirection.x == 1 && inputDirection.z == -1) {
        targetRotation = rotationY + 5 * 3.14 / 4;
    } else if(inputDirection.x == -1 && inputDirection.z == 1) {
        targetRotation = rotationY + 3 * 3.14 / 4;
    }
    currentRotation = lerpRotation(currentRotation, targetRotation, 0.3f);
    body.transform.r = glm::mat3x3(glm::rotate(glm::mat4(1.0f), currentRotation , glm::vec3(0.0f, 1.0f, 0.0f)));

    static bool jumpPressed = false;
    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS && !jumpPressed) {
        jumpPressed = true;

        // Utilisez les mêmes paramètres de vitesse et d'accélération que dans jeterCube
        float v0 = 0.8f; // vitesse initiale
        float poids = body.cubeProperties->weight;

        // Ajouter une force vers le haut pour simuler un saut
        body.cubeProperties->velocity.y += v0;

        // Mettre à jour le poids du body et l'accélération due à la gravité
        //body.cubeProperties->weight = poids;
        body.cubeProperties->acceleration = gravity * poids;
    }
    if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_RELEASE) {
        jumpPressed = false;
    }

    /*std::cout << "body x: " << body.transform.t.x << ", y: " << body.transform.t.y << ", z: " << body.transform.t.z
              << std::endl;*/
}

void jeterCube(Transform &bras1Transform, glm::vec3 upperArmEnd1, GrapheScene &grapheScene, Node* poule, GLFWwindow *window) {
    if (glfwGetKey(window, GLFW_KEY_C) == GLFW_PRESS && shotFired) {
        updateRotations = true;
        shotFired = false;
        float v0 = 0.8f; // vitesse initiale
        float poids = 0.1f;

        // Utiliser l'orientation du bras pour déterminer la direction du lancer
        glm::vec3 forward = glm::normalize(bras1Transform.r * glm::vec3(0.0f, 0.0f, -1.0f));

        glm::vec3 startPosition = bras1Transform.t + bras1Transform.r * upperArmEnd1;

        CubeProperties *cubeProperties = new CubeProperties(v0 * forward, poids, gravity * poids);

        Node *newCubeLance = new Node(
                Transform(glm::vec3(1.0f), glm::mat3x3(1.0f), startPosition),
                false, false, true, 0.1f, cubeProperties);
        grapheScene.ajoutNode(newCubeLance);
        cubesLances.push_back(newCubeLance);

    } else if (glfwGetKey(window, GLFW_KEY_C) == GLFW_RELEASE) {
        shotFired = true; // Mettre à jour l'état de la touche 'M' lorsqu'elle est relâchée
    }
    for (Node *cubeLance : cubesLances) {
        poulePrendDegats(cubeLance, poule, grapheScene, 10);
    }
}


// glfw: whenever the window size changed (by OS or user resize) this callback function executes
// ---------------------------------------------------------------------------------------------
void framebuffer_size_callback(GLFWwindow *window, int width, int height) {
    // make sure the viewport matches the new window dimensions; note that width and
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);
}
